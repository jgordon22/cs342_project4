import java.net.*; 
import java.io.*; 
import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class EchoClient3 extends JFrame implements ActionListener
{  
	private static final long serialVersionUID = 1L;
	// GUI items
	JButton sendButton;
	JButton connectButton;
	JTextField machineInfo;
	JTextField portInfo;
	JTextField message;
	JTextField sendTo;
	JTextArea history;
	JTextField recipient;
	String str;
	String UserIP;
	String serverIp;
	JCheckBox box;
	String[] users= new String[10];
	JPanel upperPanel = new JPanel ();
	JScrollPane usersOnlinePane;
	JTextArea usersOnline;


	// Network Items
	boolean privateMsg;
	boolean connected;
	Socket echoSocket;
	PrintWriter out;
	BufferedReader in;
	String UserName;
	clientThread thread;

	// set up GUI
	public EchoClient3()
	{
		super("Client");
		UserName = JOptionPane.showInputDialog(EchoClient3.this, "UserName: ");
		this.setTitle(UserName + "'s ChatBox");
		privateMsg = false;

		// get content pane and set its layout
		Container container = getContentPane();
		container.setLayout (new BorderLayout ());

		// set up the North panel
		JPanel upperPanel = new JPanel ();

		upperPanel.setLayout (new GridLayout (6,2));
		container.add (upperPanel, BorderLayout.NORTH);


		// create buttons
		connected = false;
		upperPanel.add ( new JLabel ("Message: ", JLabel.RIGHT) );
		message = new JTextField ("");
		message.addActionListener( this );
		upperPanel.add( message );

		sendButton = new JButton( "Send Message" );
		sendButton.addActionListener( this );
		sendButton.setEnabled (false);
		upperPanel.add( sendButton );
		
		connectButton = new JButton( "Connect to Server" );
		connectButton.addActionListener( this );
		upperPanel.add( connectButton );

		upperPanel.add ( new JLabel ("Server Address: ", JLabel.RIGHT) );
		try{
			InetAddress addr = InetAddress.getLocalHost();
			UserIP = addr.getHostAddress();
			machineInfo = new JTextField (UserIP);
		}
		catch(UnknownHostException e){
			UserIP ="192.168.1.86";
		}
		upperPanel.add( machineInfo);

		upperPanel.add ( new JLabel ("Server Port: ", JLabel.RIGHT) );
		portInfo = new JTextField ("56424");
		upperPanel.add( portInfo );
        
		upperPanel.add(new JLabel("enter intended recepients with a ';' in between",JLabel.RIGHT));
		recipient= new JTextField("");
		upperPanel.add(recipient);
		
		box = new JCheckBox("Public message");
		box.setSelected(true);
		box.addActionListener(this);
		upperPanel.add(box);


		history = new JTextArea ( 10, 33 );
		history.setEditable(false);
		container.add(new JScrollPane(history) ,  BorderLayout.WEST);

		usersOnline = new JTextArea ( 10, 10 );
		usersOnline.setEditable(false);
		container.add(new JScrollPane(usersOnline) ,  BorderLayout.EAST);

		setSize( 600, 300 );
		setVisible( true );

	} // end CountDown constructor

	public static void main( String args[] )
	{ 
		EchoClient3 application = new EchoClient3();
		application.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
	}

	// handle button event
	public void actionPerformed( ActionEvent event )
	{
		if ( connected && (event.getSource() == sendButton || event.getSource() == message ) )
		{

			if(box.isSelected()&& recipient.getText().isEmpty()) // message sent to everyone
			out.println("MSG "+message.getText());
			else if(!box.isSelected() &&!recipient.getText().isEmpty()){
				System.out.println(recipient.getText());
				out.println("PRIVATEMSG"+";"+message.getText()+";"+ recipient.getText());
			message.setText("");
			}
			else if(box.isSelected()&& !recipient.getText().isEmpty()){
				JOptionPane.showMessageDialog(EchoClient3.this,
					    "The box is checked and a specific user is specified!.",
					    "error",
					    JOptionPane.ERROR_MESSAGE);
			}
			else{
				JOptionPane.showMessageDialog(EchoClient3.this,
					    "Please check the box to send the message to all users or specify the users with !.",
					    "wich you want to share the message",
					    JOptionPane.ERROR_MESSAGE);
			}
		}
		else if (event.getSource() == connectButton)
		{
			if(connected){
				System.out.println("DISCONNECT " + UserName);
				out.println("DISCONNECT " + UserName);
				doManageConnection();
				usersOnline.setText("");
				history.insert("You have left the chatroom.\n", 0);
			}
			else if(!connected){
				doManageConnection();
				out.println("USERNAME " + UserName);
			}
		}

	}

	public void doManageConnection()
	{
		if (connected == false)
		{
			String machineName = null;
			int portNum = -1;
			String intended_user=null;
			try {

				machineName = machineInfo.getText();
				portNum = Integer.parseInt(portInfo.getText());
				intended_user= recipient.getText();
                String [] recipients = intended_user.split(" "); // creating an array with private recipients
				echoSocket = new Socket(machineInfo.getText(), portNum );
				out = new PrintWriter(echoSocket.getOutputStream(), true);
				in = new BufferedReader(new InputStreamReader(echoSocket.getInputStream()));
				sendButton.setEnabled(true);
				connected = true;
				connectButton.setText("Disconnect from Server");
				thread = new clientThread(this);

			} catch (NumberFormatException e) {
				history.insert ( "Server Port must be an integer\n", 0);
			} catch (UnknownHostException e) {
				history.insert("Don't know about host: " + machineName + "\n", 0);
			} catch (IOException e) {
				history.insert ("Couldn't get I/O for "
						+ "the connection to: " + machineName +"\n", 0);
			}

		}
		else
		{
			try 
			{
				connected = false;
				out.close();
				in.close();
				echoSocket.close();
				sendButton.setEnabled(false);

				connectButton.setText("Connect to Server");
			}
			catch (IOException e) 
			{
				history.insert ("Error in closing down Socket ", 0);
			}
		}


	}

	public class clientThread extends Thread{
		private EchoClient3 client;

		public clientThread(EchoClient3 c){
			client = c;
			start();
		}

		public void run(){

			try {

				String inputLine;

				while (connected){
					inputLine = client.in.readLine();
					if(inputLine.equals(client.UserName + " has joined the chatroom.")){
						client.history.insert("You have joined the chatroom.\n",0);
						client.usersOnline.insert(client.UserName + "\n", 0);
					}
					else if(!inputLine.split(" ")[0].contains(":") && inputLine.startsWith("NEWSN") && !client.UserName.equals("NEWSN")){
						UserName = JOptionPane.showInputDialog(EchoClient3.this, "Username already taken or contains spaces!\nPlease select a different username: ");
						client.out.println("USERNAME " + UserName);
						client.setTitle(UserName + "'s ChatBox");
					}
					else if(!inputLine.split(" ")[0].contains(":") && inputLine.startsWith("?*&") && !client.UserName.equals("?*&")){
						String[] userlist = inputLine.split(" ");
						for(int i=1; i<userlist.length;i++)
							if(!client.usersOnline.getText().contains(userlist[i] + "\n"))
								client.usersOnline.insert(userlist[i] + "\n", 0);
					}
					
					else if(!inputLine.contains(":") && inputLine.startsWith("?*&?*&?*&?")){
						  System.out.println(inputLine);
						  String [] rec= inputLine.split(";");
						  for(int i=2;i<rec.length;i++){
							   client.history.insert(inputLine+".\n",0);
							  }
						  }
					
					else if(!inputLine.split(" ")[0].contains(":") && inputLine.contains(" has joined the chatroom.")){
						client.history.insert(inputLine + "\n",0);
						String[] splitSN = inputLine.split(" ");
						System.out.println("SN parsed: " + splitSN[0]);
						if(!client.usersOnline.getText().contains(splitSN[0] + "\n"))
							client.usersOnline.insert(splitSN[0] + "\n", 0);
					}
					else if(!inputLine.split(" ")[0].contains(":") && inputLine.contains(" has left the chatroom.")){
						if(!inputLine.startsWith(UserName + " ")){
							client.history.insert(inputLine + "\n",0);
							String[] parsedSN = inputLine.split(" ");
							String removeSN = parsedSN[0];
							String[] splitUserList = client.usersOnline.getText().split("\n");
							String newUserList = "";
							for(int i = 0; i<splitUserList.length;i++){
								if(!splitUserList[i].equals(removeSN))
									newUserList += splitUserList[i] + "\n";
							}
							client.usersOnline.setText(newUserList);
						}

					}
					else{
						if(inputLine.length()>55){
							int numLines = inputLine.length()/55;
							String insertLines = "";
							for(int i = 0;i<numLines;i++){
								insertLines += inputLine.substring(55*i,55*(i+1)-1) + "\n";
							}
							client.history.insert(insertLines, 0);
						}
						else{
							client.history.insert(inputLine + "\n", 0);
						}
					}
				}
			} catch (IOException e) {
			}
		}
	}
} // end class EchoServer3
