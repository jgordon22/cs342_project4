import java.io.PrintWriter;
import java.net.Socket;


public class ClientUser {
	int ID;
	String name;
	Socket clientSocket;
	PrintWriter out;



	public ClientUser(String n, Socket SOCK, PrintWriter o){
		name = n;
		clientSocket = SOCK;
		out = o;

	}


	public PrintWriter getOut(){
		return out;
	}
	
	public String getName(){
		return name;
	}

}
